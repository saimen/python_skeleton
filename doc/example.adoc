= Python Skeleton : An template python project
:author: Simon Barth
:icons: font


== Introduction
_Python Skeleton_ is a template Python project that can be copied over and adapted to have a starting point for new Python projects. It contains one implementation file _example.py_ and a file containing tests for the implementation _test_example.py_. Documentation is written in _asciidoc_ format and is contained in the _doc_ subdirectory. Licensing information is given in the standard _SPDX_ format.

NOTE: My single line note

TIP: My single line tip

CAUTION: My single line caution

IMPORTANT: My single line important

WARNING: My single line warning

.Sidebar
****
A sidebar
****

[NOTE]
====
My
multiline
note
====

[TIP]
====
My
multiline
tip
====

[CAUTION]
====
My
multiline
caution
====

[IMPORTANT]
====
My
multiline
important
====

[WARNING]
====
My
multiline
warning
====



.Python Code Example
[source, python]
----
class MyClass(object):
    def __init__(self): <1>
        self.num = 0
----

<1> this is how you write a constructor in Python

.C++ Code example
[source,c++]
----
/**
 * @brief
 */
int main()
{
    return EXIT_SUCCESS;
}
----
