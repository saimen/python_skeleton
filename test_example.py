#! /usr/bin/env python

# SPDX-License-Identifier: MIT

import unittest
import unittest.mock

import example

class TestExample(unittest.TestCase):

    def setUp(self):
        self.unit_under_test = example.Example()

    def test_example_func(self):
        self.assertEqual("Example 1", self.unit_under_test.example_func())

if __name__ == '__main__':
    unittest.main()
