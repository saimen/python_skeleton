#! /usr/bin/env python

# SPDX-License-Identifier: MIT

def main():
    an_example = Example()
    print(an_example.example_func())

class Example(object):
    def example_func(self):
        return "Example {}".format(1)

if __name__ == "__main__":
    main()
